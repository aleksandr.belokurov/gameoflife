package ru.sber.gameoflife;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import ru.sber.gameoflife.core.CoreGame;
import ru.sber.gameoflife.core.InfoGame;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main extends Application {

    private static final String NAME_GAME = "Game of Life";

    private int heightMatrix = 10;
    private int widthMatrix = 10;
    private int sizeCell = 50;
    private int width;
    private int height;

    private CoreGame coreGame;
    private GraphicsContext context;
    private InfoGame infoGame;
    private Stage stage;
    private Timeline timeline;

    @Override
    public void init() throws Exception {
        super.init();
        setParams();
        infoGame = new InfoGame();
        coreGame = new CoreGame(infoGame, heightMatrix, widthMatrix);
        try {
            chooseFigure();
            infoGame.addUniverse(coreGame.getUniverse());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Selected size doesn't fit figure");
            init();
        }

    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.setTitle(NAME_GAME);
        Canvas canvas = new Canvas(width, height);
        context = canvas.getGraphicsContext2D();
        context.setFill(Color.GRAY);
        Group group = new Group(canvas);
        Scene scene = new Scene(group, width, height);
        stage.setScene(scene);
        printMatrix(coreGame.getUniverse().getMatrix());
        stage.show();

        timeline = new Timeline(new KeyFrame(Duration.millis(500), e -> run()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void run() {
        coreGame.move();
        if (infoGame.isEnd()) {
            timeline.stop();
            stage.close();
        }
        printMatrix(coreGame.getUniverse().getMatrix());
    }

    private void printMatrix(boolean[][] matrix) {
        context.clearRect(0, 0, height, width);
        for (int i = 1; i < matrix.length - 1; i++) {
            for (int j = 1; j < matrix[i].length - 1; j++) {
                if (matrix[i][j]) {
                    context.fillRect((j -1) * sizeCell, (i -1) * sizeCell, sizeCell, sizeCell);
                }
            }
        }

    }

    private void setParams() {
        heightMatrix = inputNumber("Set size row of matrix");
        widthMatrix = inputNumber("Set size column of matrix");
        sizeCell = inputNumber("Set size cell");
        height = heightMatrix * sizeCell;
        width = widthMatrix * sizeCell;
    }

    private int inputNumber(String str) {
        System.out.println(str);
        int result = 1;
        Scanner scanner = new Scanner(System.in);
        try {
            result = scanner.nextInt();
        } catch (InputMismatchException e) {
            inputNumber("You incorrect input. Please try again.");
        }
        return result;
    }

    private void chooseFigure() throws ArrayIndexOutOfBoundsException {
        System.out.println("Choose figure  : \n 1: Random \n 2: Blinker \n 3: Glider ");
        int numberFigure = inputNumber("Input number : ");
        switch (numberFigure) {
            case (1) : {
                coreGame.getUniverse().fillRandomMatrix();
                break;
            }
            case (2) : {
                coreGame.getUniverse().fillBlinker();
                break;
            }
            case (3) : {
                coreGame.getUniverse().fillGlider();
                break;
            }
        }
    }

}
