package ru.sber.gameoflife.core;


import ru.sber.gameoflife.model.Universe;

public class CoreGame {

    private InfoGame infoGame;
    private Universe universe;

    public CoreGame(InfoGame infoGame, int height, int width) {
        universe = new Universe(height, width);
        this.infoGame = infoGame;
    }



    public Universe getUniverse() {
        return universe;
    }

    public void move() {
        int numberLiveCell = 0;
        int height = universe.getHeight();
        int width = universe.getWidth();
        boolean[][] newMatrix = universe.cloneMatrix();
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                int numberLivingAboutCell = countLiveCell(i, j);
                if (newMatrix[i][j]) {
                    numberLiveCell++;
                }
                if (numberLivingAboutCell == 3){
                    newMatrix[i][j] = true;
                } else if (numberLivingAboutCell > 3 || numberLivingAboutCell < 2) {
                    newMatrix[i][j] = false;
                }
            }
        }
        universe.setMatrix(newMatrix);
        infoGame.setInfoAboutGame(universe, numberLiveCell);

    }

    private int countLiveCell(int indexRow, int indexColumn) {
        int numberLiveCell = 0;
        boolean[][] matrix = universe.getMatrix();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int newIndexRow = indexRow + i;
                int newIndexColumn= indexColumn + j;
                if (!(i == 0 && j == 0) && matrix[newIndexRow][newIndexColumn]
                ) {
                    numberLiveCell++;
                }
            }
        }
        return numberLiveCell;
    }

}
