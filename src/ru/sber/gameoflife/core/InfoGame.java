package ru.sber.gameoflife.core;


import ru.sber.gameoflife.model.Universe;

import java.util.HashSet;
import java.util.Set;

public class InfoGame {

    private final Set<Universe> allUniverses = new HashSet<>();
    private boolean isEnd = false;

    public void setInfoAboutGame(Universe universe, int numberCell) {
        if (numberCell == 0) {
            isEnd = false;
        } else {
            checkExistsUniverse(universe);
        }
    }
    private void checkExistsUniverse(Universe universe) {
        if (allUniverses.contains(universe)) {
            isEnd = true;
        } else  {
            allUniverses.add(universe);
        }
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void addUniverse(Universe universe) {
        this.allUniverses.add(universe);
    }

}
