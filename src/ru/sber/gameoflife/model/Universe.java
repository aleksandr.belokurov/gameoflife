package ru.sber.gameoflife.model;

import java.util.Arrays;

public class Universe {

    private static final float CHANCE_LIVE_CELL = 0.7f;
    private static final int NUMBER_ADDITIONAL_FIELD_MATRIX = 2;

    boolean[][] matrix;
    int height;
    int width;

    public boolean[][] cloneMatrix() {
        boolean[][] newMatrix = new boolean[height][width];
        for (int i = 0; i < matrix.length; i++) {
            newMatrix[i] = matrix[i].clone();
        }
        return newMatrix;
    }

    public Universe(int height, int width) {
        matrix = new boolean[height + NUMBER_ADDITIONAL_FIELD_MATRIX][width + NUMBER_ADDITIONAL_FIELD_MATRIX];
        this.height = height + NUMBER_ADDITIONAL_FIELD_MATRIX;
        this.width = width + NUMBER_ADDITIONAL_FIELD_MATRIX;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setMatrix(boolean[][] matrix) {
        this.matrix = matrix;
        fillExtendedMatrix();
    }

    public boolean[][] getMatrix() {
        return matrix;
    }

    public void fillGlider() throws ArrayIndexOutOfBoundsException {
        matrix[1][2] = true;
        matrix[2][3] = true;
        matrix[3][1] = true;
        matrix[3][2] = true;
        matrix[3][3] = true;
    }

    public void fillBlinker() throws ArrayIndexOutOfBoundsException {
        matrix[1][2] = true;
        matrix[2][2] = true;
        matrix[3][2] = true;
    }

    /**
     * fill in central part with random value.
     */
    public void fillRandomMatrix() throws ArrayIndexOutOfBoundsException {
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                if (Math.random() > CHANCE_LIVE_CELL) {
                    matrix[i][j] = true;
                }
            }
        }
    }

    /**
     * fill edges matrix.
     */
    private void fillExtendedMatrix() {
        matrix[0] = matrix[height - NUMBER_ADDITIONAL_FIELD_MATRIX].clone();
        matrix[height - 1] = matrix[1].clone();

        for (int i = 1; i < height - 1; i++) {
            matrix[i][0] = matrix[i][width - NUMBER_ADDITIONAL_FIELD_MATRIX];
            matrix[i][width - 1] = matrix[i][1];
        }

        matrix[0][0] = matrix[height - NUMBER_ADDITIONAL_FIELD_MATRIX][width - NUMBER_ADDITIONAL_FIELD_MATRIX];
        matrix[height - 1][width - 1] = matrix[1][1];
        matrix[height - 1][0] = matrix[1][width - 2];
        matrix[0][width - 1] = matrix[height - NUMBER_ADDITIONAL_FIELD_MATRIX][1];
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        Universe universe = (Universe) o;
        return Arrays.deepEquals(matrix, universe.matrix);
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(matrix);
    }
}
